import java.util.Scanner;
/**
 * 
 * @author aliraissian
 *
 */


public class House {
	 String colorOfWalls;
	 String floorType;
	 String windows;
	  int numberOfWindows;
	  /**
	   * I will use void to instruct the user and store the inputs in the varibales above.
	   * i will use this. to specify which attributes i will use and not to confuse with the new varibals that will replace the old one
	   * 
	   */

	
	public void readInput(){// a void methoid to promt the user about the program
		Scanner keyboard = new Scanner(System.in);
		System.out.println("what color you want the walls to be?");
		System.out.print("wall color : ");
		this.colorOfWalls=keyboard.nextLine();
		System.out.println("Choose the floor type from the following\n (Hard wood) (carpet) (tiled)");
		this.floorType=keyboard.nextLine();
		System.out.println("do you want windows?\n (yes,no)");
		this.windows=keyboard.nextLine();
		if(this.windows.equalsIgnoreCase("yes")){
			System.out.println("Ok good! how many would you like ?");
			this.numberOfWindows=keyboard.nextInt();
		}
		else 
			this.windows="No windows";
	}
	/**
	 * 
	 * @return color of the walls
	 */
	
	
	public String getWallColor(){//get methods to get the desirable methods
		return colorOfWalls;
	}
	/**
	 * 
	 * @return windows if its a yes or no, but i wont be needing it.
	 */
	public String getWindows(){
		return windows;
	
	}
	/**
	 * 
	 * @return floor type 
	 */
	public String getFloorType(){
		return floorType;
	}
	/**
	 * 
	 * @return number of the windows
	 */
public int getNumberOfWindows(){
	return numberOfWindows;
}
/**
 * 
 * @param wallColor sets it to the new wall color for the desirable class, so i can change it whenever i want.
 */
public void setWallColor(String wallColor){//sets to set the vriables to the new varibales or not to confuse the old varibvales with new ones.
	this.colorOfWalls= wallColor;
	
}
/**
 * 
 * @param floorType sets the the new floor type.
 */
public void setFloorType(String floorType){
	this.floorType=floorType;
}
/**
 * 
 * @param windows sets to new windows
 * @param windowNumbers sets to new number of windows
 */
public void setWindows(String windows, int windowNumbers){
	this.windows=windows;
	this.numberOfWindows=windowNumbers;
	}
}
