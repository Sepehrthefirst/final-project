import javax.swing.JOptionPane;
/**
 * 
 * @author Sepehr Raissian
 * This class will prompt the user to Deposit and or Withdraw from the Savings or checkings account and will also show the balance.
 *
 */

public class ATM {

	int savingsDeposit;
	int savingsWithdraw;
	int checkingsDeposit;
	int checkingsWithdraw;
	int savingsBalance;
	int checkingsBalance;
	
	public ATM(){
		
	}
/**
 * This is the overload constructor that will prompt the user and calculate all the transactions.
 */
	public void Purchase(){
		
	
	
		String [] DepositOrWithdraw ={"Deposit","Withdraw"};
		String [] SavingOrChecking={"Saving","checking"};
	//	String [] YesOrNo={"YES","NO"};
		String SavingsOrCheckingsInput= (String)JOptionPane.showInputDialog(null, "Choose an account type","Account",JOptionPane.QUESTION_MESSAGE, null, SavingOrChecking, SavingOrChecking[0]);
		
		if(SavingsOrCheckingsInput.equals(SavingOrChecking[0])){
			String WithdrawOrDepositInput =(String)JOptionPane.showInputDialog(null, "Would you like to Deposit or Withdraw","Account",JOptionPane.QUESTION_MESSAGE, null, DepositOrWithdraw, DepositOrWithdraw[0]);
			
			if(WithdrawOrDepositInput.equals(DepositOrWithdraw[0])){
			int sDeposit = Integer.parseInt(JOptionPane.showInputDialog(null,"How much would you like to deposit?")); 	
			savingsDeposit=savingsDeposit+sDeposit;
			savingsBalance=savingsBalance+sDeposit;
			}
			
			if(WithdrawOrDepositInput.equals(DepositOrWithdraw[1])){
				int sWithdraw = Integer.parseInt(JOptionPane.showInputDialog(null,"How much would you like to Withdraw?"));	
				savingsWithdraw=savingsWithdraw+sWithdraw;
				savingsBalance=savingsBalance-sWithdraw;
																}
		
										}//ifSavings
		
		if(SavingsOrCheckingsInput.equals(SavingOrChecking[1])){
			String WithdrawOrDepositInput =(String)JOptionPane.showInputDialog(null, "Would you like to Deposit or Withdraw","Account",JOptionPane.QUESTION_MESSAGE, null, DepositOrWithdraw, DepositOrWithdraw[0]);
			
			if(WithdrawOrDepositInput.equals(DepositOrWithdraw[0])){
			int cDeposit = Integer.parseInt(JOptionPane.showInputDialog(null,"How much would you like to deposit?")); 	
			checkingsDeposit=checkingsDeposit+cDeposit;
			checkingsBalance=checkingsBalance+cDeposit;
			}
			
			if(WithdrawOrDepositInput.equals(DepositOrWithdraw[1])){
				int cWithdraw = Integer.parseInt(JOptionPane.showInputDialog(null,"How much would you like to Withdraw?"));	
				checkingsWithdraw=checkingsWithdraw+cWithdraw;
				checkingsBalance=checkingsBalance-cWithdraw;
																}
		
										}//ifDeposit

	//	String BalanceView= (String)JOptionPane.showInputDialog(null, "Would you like to see your balance?","Account",JOptionPane.QUESTION_MESSAGE, null, YesOrNo, YesOrNo[0]);
		//if(BalanceView.equals(YesOrNo[1]))
			 

		
	
	}
/**
 * This will return the savingsDeposit.
 * @return getSavingsDeposit
 */
	public int getSavingsDeposit() {
		return savingsDeposit;
	}
/**
 * This will set a new savingsDeposit and replaced it with the savingsDeposit.
 * @param savingsDeposit
 */
	public void setSavingsDeposit(int savingsDeposit) {
		this.savingsDeposit = savingsDeposit;
	}
/**
 * This will return the savingsWithdraw.
 * @return getSavingsWithdraw
 */
	public int getSavingsWithdraw() {
		return savingsWithdraw;
	}
/**
 * This will set a new savingsWithdraw and replace it with the old savingsWithdraw.
 * @param savingsWithdraw
 */
	public void setSavingsWithdraw(int savingsWithdraw) {
		this.savingsWithdraw = savingsWithdraw;
	}
/**
 * This will return the checkingDeposit.
 * @return getCheckingDeposit
 */
	public int getCheckingsDeposit() {
		return checkingsDeposit;
	}
/**
 * This will set a new checkingsDeposit and replace it with the old checkingsDeposit.
 * @param checkingsDeposit
 */
	public void setCheckingsDeposit(int checkingsDeposit) {
		this.checkingsDeposit = checkingsDeposit;
	}
/**
 * This will return the checkingsWithdraw.
 * @return checkingsWithdraw
 */
	public int getCheckingsWithdraw() {
		return checkingsWithdraw;
	}
/**
 * This will set a new checkingsWithdraw and replaced it with the old checkingsWithdraw.
 * @param checkingsWithdraw
 */
	public void setCheckingsWithdraw(int checkingsWithdraw) {
		this.checkingsWithdraw = checkingsWithdraw;
	}
/**
 * This will return the savingsBalance.
 * @return SavingsBalance
 */
	public int getSavingsBalance() {
		return savingsBalance;
	}
/**
 * This will set a new savingsBalance and replaced it with the old savingsBalance.
 * @param savingsBalance
 */
	public void setSavingsBalance(int savingsBalance) {
		this.savingsBalance = savingsBalance;
	}
/**
 * This will return the checkingsBalance.
 * @return checkingsBalance
 */
	public int getCheckingsBalance() {
		return checkingsBalance;
	}
/**
 * This will set a new checkingsBalance and replaced it with old checkingsBalance.
 * @param checkingsBalance
 */
	public void setCheckingsBalance(int checkingsBalance) {
		this.checkingsBalance = checkingsBalance;
	}
}
	
	
	
