import javax.swing.JOptionPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;
/**
 * 
 * @author Sepehr Raissian
 * This class will be used to create multiple accounts using arrays
 *
 */

public class Registration {
	
	String UserName;
	String PassWord;
	String Confirm;
	String Email;
	String PhoneNumber;
	int passWordTrials;
	int emailTrials;
	int passWordStop;
	int emailStop;
	/**
	 * Defualt constructor
	 */

	public Registration() {

	}
/**
 * readUserName will read a userName and store it in a variable called UserName in a class of readUserName.
 * with having a do while the userName cannot be blank and it keep on promoting until the user enter a userName.
 */
	public void readUserName() {
		do {
			UserName = JOptionPane.showInputDialog(null,
					"Enter the desriable Username");
		} while (UserName.equals(""));

	}
/**
 * This will return the userName 
 * @return getUserName
 */
	public String getUserName() {
		return UserName;
	}
/**
 * This will set a new userName and replace it with the old userName
 * @param userName
 */
	public void setUserName(String userName) {
		UserName = userName;
	}
	/**
	 * This will return the password.
	 * @return getPassWord
	 */

	public String getPassWord() {
		return PassWord;
	}
/**
 * This will set a new password and replace it with the old password
 * @param passWord
 */
	public void setPassWord(String passWord) {
		PassWord = passWord;
	}
/**
 * This will return the confirmed password.
 * @return getConfirm
 */
	public String getConfirm() {
		return Confirm;
	}
/**
 * This will set a new confirmed password and replace it with the old confirmed password.
 * @param confirm
 */
	public void setConfirm(String confirm) {
		Confirm = confirm;
	}
/**
 * This will return the Email.
 * @return Email
 */
	public String getEmail() {
		return Email;
	}
	/**
	 * This will set a new email and replace it with the old email.
	 * @param email
	 */

	public void setEmail(String email) {
		Email = email;
	}
	/**
	 * This will return the phoneNumber.
	 * @return phoneNumber
	 */

	public String getPhoneNumber() {
		return PhoneNumber;
	}
/**
 * This will set a new PhoneNUmber and replace it with the old PhoneNumber.
 * @param phoneNumber
 */
	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
/**
 * This is the overload constructor.
 * This for reading a passWord and a Confirmed passWord and to see if the user has entered the correct password both times, if not the user will be blocked after three trials.
 * 
 */
	public void readPassWord() {
		passWordTrials = 3;
		passWordStop = 0;

		PassWord = JOptionPane.showInputDialog(null, "Enter your Password");
		Confirm = JOptionPane.showInputDialog(null, "ReEnter your Password");
		if (PassWord.equals(Confirm)){
			JOptionPane.showMessageDialog(null, "Good you may proceed");
		passWordStop = 1;}
		if (!(PassWord.equals(Confirm))) {
			JOptionPane.showMessageDialog(null,"Confrimed Password was incorect\nenter your password again");
			while( (passWordTrials > 0)&&(passWordStop==0)) {
				
				Confirm = JOptionPane.showInputDialog(null,"Enter your Confirmed password\n you have : "+ passWordTrials + " trials left");
				passWordTrials--;
				if (Confirm.equals(PassWord)) {
					JOptionPane.showMessageDialog(null, "Good you may proceed");
					passWordStop = 1;

				}
				if ((passWordTrials == 0) && (passWordStop == 0)) {

					JOptionPane.showMessageDialog(null,	"your account has been blocked ");
				}
			}// while
		}// if
	}
/**
 * This another overload constructor that will read an email and save it in a variable called email in the class of readEmail.
 */
	public void readEmail() {
		emailStop = 0;
		emailTrials = 3;
		Email = JOptionPane.showInputDialog(null, "Enter your Email");

		if (!(Email.equals(""))){
			JOptionPane.showMessageDialog(null, "Good you may proceed");
			emailStop = 1;}
		if ((Email.equals(""))) {
			JOptionPane.showMessageDialog(null, "Email cannot be blank");
			while ((emailTrials > 0)&&(emailStop==0)) {
				Email = JOptionPane.showInputDialog(null,
						"Enter your Email\n you have : " + emailTrials
								+ " trials left");
				emailTrials--; 
				if (!(Email.equals(""))) {
					emailStop = 1;
					JOptionPane.showMessageDialog(null, "Good you may proceed");

						}
				if ((emailTrials == 0) && (emailStop == 0)) {
					JOptionPane.showMessageDialog(null,
							"your account has been blocked ");}
				}
			}// while
		}// if

	
/**
 * This will read the PhoneNumber and store it in a variable called PhoneNumber in the class of readPhoneNumber.
 */
	public void readPhoneNumber() {
		PhoneNumber = JOptionPane.showInputDialog(null,
				"Enter your phone number");

	}
/**
 * this will print all the information that so far has been stored in this classes.
 * @return printInfo
 */
	public String printInfo()
	{
		String output = "";

		output += "Your username is : " + UserName + "\n";
		output += "Your password is : " + PassWord + "\n";
		output += "Your emaill is : " + Email + "\n";
		output += "Your phone number is : " + PhoneNumber + "\n";
		return output;
	}
	/**
	 * this will by default return the userName.
	 */
	public String toString() {
		return UserName;
	}

}