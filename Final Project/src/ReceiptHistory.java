import javax.swing.JOptionPane;

/**
 * 
 * @author Sepehr Raissian This class called ReceiptHistory will print out all
 *         the activities that has been for this whole Project.
 * 
 */
public class ReceiptHistory {
	CreditCard user;
	Registration user1;
	ATM user2;

	/**
	 * by using get setters I can return all the variables that has been used to
	 * store variables in them as promoting the user.
	 */
	public ReceiptHistory(CreditCard u, Registration u1, ATM u2) {
		user = u;
		user1 = u1;
		user2 = u2;

	}

	public void printReciept() {
		JOptionPane
				.showMessageDialog(
						null,
						"your username is : "
								+ user1.getUserName()
								+ "\nyour password is : "
								+ user1.getPassWord()
								+ "\nyour Email is : "
								+ user1.getEmail()
								+ "\nyour phone number is : "
								+ user1.getPhoneNumber()
								+ "\nyour Credit card number is : "
								+ user.getCreditCardNumber()
								+ "\nyour minimum payment is : "
								+ user.getMinimumPayment()
								+ "\nyour Checkings Balance is : "
								+ user2.getCheckingsBalance()
								+ "\nyou Savings Balance : "
								+ user2.getSavingsBalance()
								+ "\nThe total amount of Depoits you did to your Checking account is : "
								+ user2.getCheckingsDeposit()
								+ "\nThe total amount of Withdraws you did from your Checking account is : "
								+ user2.getCheckingsWithdraw()
								+ "\nThe total amount of Depoits you did to your Savings account is : "
								+ user2.getSavingsDeposit()
								+ "\nThe total amount of Withdraws you did to your Savings account is :"
								+ user2.getSavingsWithdraw()
								+ "\nYour sum of Transaction is "
								+ user.getSum() + "\n" + user.getLargest()
								+ " is the largest Transaction you have done"
								+ "\n" + "your average Transaction is "
								+ user.getAvrage()
								+ "\nYour new minimum payment per month is :"
								+ user.getNewMinimumPayment());
	}

}
