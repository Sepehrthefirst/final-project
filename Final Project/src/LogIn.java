import javax.swing.JOptionPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;
/**
 * 
 * @author Sepehr Raissian
 * This class will be used to log in according to the accounts created by the user in the Registration class.
 *
 */
public class LogIn {
	int PassWordCorrect=0;
	int x=3;
	Registration currentAccount; 
	
	public  LogIn(Registration [] usernames){
		
		ImageIcon icon= new ImageIcon("IMG_0098.jpg", "java icon");
		currentAccount = (Registration)JOptionPane.showInputDialog(null,"Choose an account type","Account",JOptionPane.QUESTION_MESSAGE, icon, usernames, usernames[0]);
		
		String inputPassWord=(String)JOptionPane.showInputDialog(null," Enter your password");
		
		
		int index = -1;
		for(int i = 0; i < usernames.length; i++)
		{
			if(currentAccount.getUserName().equals(usernames[i].getUserName()))
			{
				index = i;
			}
		}
		
		// PASSWORD CHECK
		if(inputPassWord.equals(usernames[index].getPassWord())){
			JOptionPane.showMessageDialog(null, "Good");
			PassWordCorrect=1;
		}
		if(!(inputPassWord.equals(usernames[index].getPassWord()))){
			while((x>0)&&(PassWordCorrect==0)){
				inputPassWord=(String)JOptionPane.showInputDialog(null," Enter your password"+"\nyou have : "+x+" left");
				x--;
				if(inputPassWord.equals(usernames[index].getPassWord())){
					JOptionPane.showMessageDialog(null, "Good");
					PassWordCorrect=1;}
			}
		}
		
	}
	
	public Registration getCurrAccount(){
		return currentAccount;
	}

}
