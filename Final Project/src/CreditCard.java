import javax.swing.JOptionPane;
/**
 * 
 * @author Sepehr Raissian
 * This class will be used to create a credit card and credit card number and to calculate the minimum payment per month.
 *
 */

public class CreditCard {
	int creditCardNumber;
	double minimumPayment;
	double sum=0,TransactionAmount=0;
	double newMinimumPayment=0;
	double smallest = Double.MAX_VALUE;
	double largest = -1;
	double count=0;
	double avrage=0;
	/**
	 * This is the overload constructor 
	 * It will prompt the user if it wishes to create a credit card.
	 */
	public CreditCard(){
		
	}
	public void CreateCredit(){
		String [] YOrN={"YES","NO"};
		String input = (String) JOptionPane.showInputDialog(null,"Would you like to Create a credit Card ?","Credit Card",JOptionPane.QUESTION_MESSAGE,null,YOrN,YOrN[0]);
		
	if(input.equals(YOrN[0])){
		double number= 100000000*Math.random();
		creditCardNumber = (int)Math.round(number);
		JOptionPane.showMessageDialog(null, "Your Credit card number is : "+creditCardNumber);
		double credit = Double.parseDouble(JOptionPane.showInputDialog(null,"How much you intent to spend for the first month?(interest Rate is 2%)"));
		minimumPayment=credit*0.02;
		JOptionPane.showMessageDialog(null, "Your minimum payment per month is : $ "+minimumPayment);
		String Transaction = (String) JOptionPane.showInputDialog(null,"Are you going to do a Transaction today ?","Transaction",JOptionPane.QUESTION_MESSAGE,null,YOrN,YOrN[0]);
		if(Transaction.equals(YOrN[0])){
			String Continue=(String)JOptionPane.showInputDialog(null,"Continue?","Transaction",JOptionPane.QUESTION_MESSAGE,null,YOrN,YOrN[0]);
			while((TransactionAmount>=0)&&(Continue.equals(YOrN[0]))){
			double TransactionAmount = Double.parseDouble(JOptionPane.showInputDialog(null,"How much did you spend today ?"));
			if(TransactionAmount>0)
			{	count++;
				sum=sum+TransactionAmount;
				 if (largest<number) 
					largest=TransactionAmount;
				
				 if (smallest>TransactionAmount)	
					 smallest=TransactionAmount;
				
				}//if
			 Continue=(String)JOptionPane.showInputDialog(null,"Continue?","Transaction",JOptionPane.QUESTION_MESSAGE,null,YOrN,YOrN[0]);
					}//while transaction
			 avrage=sum/count;
			 newMinimumPayment=sum*0.02;
			 JOptionPane.showMessageDialog(null,"Your sum of Transaction is "+sum + "!!");	
				JOptionPane.showMessageDialog(null,largest+" is the largest Transaction you have done");
			JOptionPane.showMessageDialog(null,smallest+" is the smallest Transaction you have done");
				JOptionPane.showMessageDialog(null,"your average Transaction is " + avrage);
				JOptionPane.showMessageDialog(null, "Your new minimum payment per month is :"+newMinimumPayment);

		}//if input.equals(YOrN[0])


		
	}
	}
	/**
	 * This will return the first minimum payment per month.
	 * @return getNewCredit
	 */
	public double getMinimumPayment() {
		return minimumPayment;
	}
	/**
	 * This will set a new minimumPayment and replace it with the old minimumPayment
	 * @param newCredit
	 */
	public void setNewCredit(double minimumPayment) {
		this.minimumPayment = minimumPayment;
	}
	/**
	 * This will return the sum of the transactions done by the user. 
	 * @return getSum
	 */
public double getSum() {
		return sum;
	}
/**
 * This will set a new sum and replace it with the old sum.
 * @param sum
 */
	public void setSum(double sum) {
		this.sum = sum;
	}
/**
 *This will return the newMinimumPayment. 
 * @return newSum
 */
	public double getNewMinimumPayment() {
		return newMinimumPayment;
	}
	/**
	 * This will set a newSnewMinimumPaymentum and replace it with the old newMinimumPayment.
	 * @param newSum
	 */
	public void setNewSum(double newMinimumPayment) {
		this.newMinimumPayment = newMinimumPayment;
	}
	/**
	 * This will return the smallest Transaction payment done.
	 * @return smallest
	 */
	public double getSmallest() {
		return smallest;
	}
	/**
	 * This will set a new smallest and replace it with the old smallest.
	 * @param smallest
	 */
	public void setSmallest(double smallest) {
		this.smallest = smallest;
	}
	/**
	 * This will return the Largest Transaction Payment done by the user. 
	 * @return largest
	 */
	public double getLargest() {
		return largest;
	}
	/**
	 * This will set a new largest and replace it with the old largest.
	 * @param largest
	 */
	public void setLargest(double largest) {
		this.largest = largest;
	}
	/**
	 * This will return the average transaction payment done by the user.
	 * @return average
	 */
	public double getAvrage() {
		return avrage;
	}
	/**
	 * This will set a new avrage and replace it with the old avrage.
	 * @param avrage
	 */
	public void setAvrage(double avrage) {
		this.avrage = avrage;
	}
/**
 * This will return the CreditCardNUmber.
 * @return getCreditCardNumber
 */
	public int getCreditCardNumber() {
		return creditCardNumber;
	}
/**
 * This will set a new CreditCardNumber and replace the old CreditCardNumber.
 * @param creditCardNumber
 */
	public void setCreditCardNumber(int creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	/**
	 * This will return the MinimumPayment per month of the user.
	 * @return getMinimumPayment
	 */

	
	

}
